#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 20
#SBATCH --mem=150G
#SBATCH --qos=general
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general

module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.25
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/EnTAP --runP --ini entap_config.ini -i combine_pep.pep -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.200.dmnd --threads 20
