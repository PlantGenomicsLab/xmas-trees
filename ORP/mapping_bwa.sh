#!/bin/bash
#SBATCH --job-name=BWA
#SBATCH -n 1
#SBATCH -c 22
#SBATCH --mem=50G
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bwa/0.7.17
module load samtools

bwa index -a bwtsw /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/NEW/EnTAP/Contam_Removal/no_contam.fasta

#Run against every library
bwa mem -t 2 /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/NEW/EnTAP/Contam_Removal/no_contam.fasta /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R2.fastq > 1.sam
samtools view -b -S 1.sam > 1.bam
samtools sort -o 1.sort.bam 1.bam
samtools index 1.sort.bam
samtools flagstat 1.sort.bam > 1.txt


