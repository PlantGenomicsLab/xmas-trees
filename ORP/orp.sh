#!/bin/bash
#SBATCH --job-name=balsam_ORP
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=110G
#SBATCH --mail-type=END
#SBATCH --mail-user=
#SBATCH -o test_%j.out
#SBATCH -e test_%j.err

#BEFORE RUNNING SCRIPT:

#Add this to the end of the .bashrc file in your home directory (getting the correct conda initialized):
#/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/etc/profile.d/conda.sh
#Make sure this is in your .bash_profile:
#PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/bin
#PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/OrthoFinder/
#PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/orp-transrate
#PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/transabyss


#You must then activate the conda environment in an interactive session:
#bash$ srun --partition=general --qos=general --pty bash
#bash$ conda activate orp
#Environment will look like this:
#(orp)$bash

#RUN SCRIPT: 
#(orp)$bash sbatch orp.sh

hostname
echo "\nStart time:"
date

#ORP does not have a module load option, thus you need to use this path to run the full analysis:
/labs/Wegrzyn/local_software/Oyster_River_Protocol/oyster.mk main CPU=24 MEM=110 \
  STRAND=RF \
#Specify path for reads of interest of sample (species dependent)
  READ1=/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R1.fastq\
  READ2=/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R2.fastq\
  LINEAGE=eukaryota_odb10 \
#label for species of interest + sample (i.e. 1.Final = 1)
  RUNOUT=1.balsam

#Repeat script if multiple samples
echo "\nEnd time:"
date
