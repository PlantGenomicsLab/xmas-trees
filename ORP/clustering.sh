#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 40
#SBATCH --mem=150G
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "vsearch"

module load vsearch/2.4.3

#Use concatenated TransDecoder CDS output as cluster_fast input
vsearch --threads 8 --log LOGFile \
        --cluster_fast ../combine_300.cds \
        --id 0.95 \
        --centroids centroids.fasta \
        --uc clusters.uc 
