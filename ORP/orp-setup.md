<h3>How to set up and run ORP:

First, you need to create and edit two files in your /home/CAM/user directory using touch.

```
touch .bash_profile
nano .bash_profile
```

Once you use touch to create the file, you can use whichever text editor you would prefer. I prefer nano. Once in the .bash_profile file, paste this block of code in it.

```
PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/bin
PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/OrthoFinder/
PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/orp-transrate
PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/transabyss
export TMPDIR=/labs/Wegrzyn/Xmas/temp/
```

Save the file and exit. Next step is the .bash_rc file.

```
touch .bash_rc
nano .bash_rc
```

And then paste this line of code into the .bash_rc file

`/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/etc/profile.d/conda.sh`

After that, you need to restart your shell. Once you restart your shell, enter an interactive session

`srun --partition=general --qos=general --pty bash`

and then run these sets of commands

```
conda activate
conda init bash
source activate orp
```

If everything worked correctly, you should have an (orp) to the left of your bash, and you can now successfully run ORP. Goto where the scripts you want to run are, and run them! After these steps, all you need to do in the future to run orp would be:

`source activate orp`

