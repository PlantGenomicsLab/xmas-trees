#!/bin/bash
#SBATCH --job-name=transdecoder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load hmmer/3.2.1
module load TransDecoder/5.3.0

#Run TransDecoder against each library
TransDecoder.LongOrfs -t ../38.fasta
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm 38.fasta.transdecoder_dir/longest_orfs.pep
TransDecoder.Predict -t ../38.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout 

TransDecoder.LongOrfs -t ../31.fasta
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm 31.fasta.transdecoder_dir/longest_orfs.pep
TransDecoder.Predict -t ../31.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout 

TransDecoder.LongOrfs -t ../5.fasta
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm 5.fasta.transdecoder_dir/longest_orfs.pep
TransDecoder.Predict -t ../5.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout

TransDecoder.LongOrfs -t ../39.fasta
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm 39.fasta.transdecoder_dir/longest_orfs.pep
TransDecoder.Predict -t ../39.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout 

TransDecoder.LongOrfs -t ../21.fasta
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm 21.fasta.transdecoder_dir/longest_orfs.pep
TransDecoder.Predict -t ../21.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout 
