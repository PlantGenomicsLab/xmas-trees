#!/bin/bash
#SBATCH --job-name=kallisto_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=40G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
##SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "kallisto_index"

module load kallisto/0.46.1

kallisto index -i balsam_index /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/NEW/EnTAP/Contam_Removal/no_contam.fasta


