#!/bin/bash
#SBATCH --job-name=rnaQuast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "rnaQUAST"

module load rnaQUAST/2.2.0
module load GeneMarkS-T/5.1

python /isg/shared/apps/rnaQUAST/2.2.0/rnaquast-2.2.0/rnaQUAST.py --transcripts ../combine_300.cds \
        --gene_mark \
        --threads 8 \
        --output_dir results_transdecoder


python /isg/shared/apps/rnaQUAST/2.2.0/rnaquast-2.2.0/rnaQUAST.py --transcripts /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/combine_300.fasta \
        --gene_mark \
        --threads 8 \
        --output_dir results_orp
