#!/bin/bash
#SBATCH --job-name=busco_balsam
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err


module load busco/5.0.0


busco -i /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/NEW/EnTAP/Contam_Removal/no_contam.fasta -l viridiplantae_odb10 -o buscoClustering_nocontam -m Transcriptome 

busco -i /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/NEW/ORP/combine_300.fasta -l viridiplantae_odb10 -o buscoORP -m Transcriptome 
