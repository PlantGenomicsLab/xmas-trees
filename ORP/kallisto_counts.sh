#!/bin/bash
#SBATCH --job-name=kallisto_counts_1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname

module load kallisto/0.46.1

#Run kallisto quant against each library

kallisto quant -i balsam_index \
        -o 1\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R2.fastq


kallisto quant -i balsam_index \
        -o 2\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/2.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/2.Final.R2.fastq


kallisto quant -i balsam_index \
        -o 11\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/11.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/11.Final.R2.fastq

kallisto quant -i balsam_index \
        -o 12\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/12.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/12.Final.R2.fastq


kallisto quant -i balsam_index \
        -o 3\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/3.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/3.Final.R2.fastq

