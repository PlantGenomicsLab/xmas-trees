# Script to run spades.sh on selected libraries

####
tree="balsam"
####

libraries=/labs/Wegrzyn/Transcriptomics/xmas_trees/NAR/poola/data/reads/$tree/trimmedReads/trimmed_libraries.txt
lib_length=$(< $libraries wc -l)



##### If unchanged, this will run rnaSPAdes on all libs at the same time
##### Change "lower" and "upper" to select how many libs you want to run at the same time
lower=1
upper=$lib_length
#####


counter=1

while read lib
do
	if ((counter >= lower)) && ((counter <= upper))
	then
		sbatch run_spades.sh $lib
	fi
	let "counter += 1"
done < $libraries
