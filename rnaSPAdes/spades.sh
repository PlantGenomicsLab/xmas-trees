#!/bin/bash
#SBATCH --job-name=spades
#SBATCH -o output_files/spades-%j.output
#SBATCH -e error_files/spades-%j.error
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G

# Script for running rnaSPAdes
# Create directories "output_files" and "error_files" before running script

echo `hostname`

module load SPAdes/3.14.1

readsDir=/labs/Wegrzyn/Transcriptomics/xmas_trees/NAR/poola/data/reads/balsam/trimmedReads

# Library given in command line
lib=$1


if [[ ! -d output_folders ]]
then
	mkdir output_folders
fi


rnaspades.py \
-1 $readsDir/${lib}R1.fastq \
-2 $readsDir/${lib}R2.fastq \
-o output_folders/${lib}output
