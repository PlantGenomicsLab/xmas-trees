tree="balsam"

libraries=/labs/Wegrzyn/Transcriptomics/xmas_trees/NAR/poola/data/reads/$tree/trimmedReads/trimmed_libraries.txt
lib_length=$(< $libraries wc -l)


##### If unchanged, this will run rnaSPAdes on all libs at the same time
##### Change "lower" and "upper" to select how many libs you want to run at the same time
lower=1
upper=$lib_length
#####

# K-mer value given in command line
k_val=$1


counter=1

while read lib
do
        if ((counter >= lower)) && ((counter <= upper))
        then
                sbatch soap.sh $lib $k_val
        fi
        let counter++
done < $libraries

