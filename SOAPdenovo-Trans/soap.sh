#!/bin/bash
#SBATCH --job-name=soap
#SBATCH -o output_files/soap-%j.output
#SBATCH -e error_files/soap-%j.error
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G

# Script for running SOAPdenovo-Trans
# Create directories "output_files" and "error_files" before running script


echo `hostname`

####
tree="balsam"
####

export PATH=$HOME/SOAPdenovo-Trans:$PATH

readsDir=/labs/Wegrzyn/Transcriptomics/xmas_trees/NAR/poola/data/reads/$tree/trimmedReads

# Library and k-mer value given in command line
lib=$1
k=$2


q1=$readsDir/${lib}R1.fastq
q2=$readsDir/${lib}R2.fastq


# Creates output directory and moves to that directory
mkdir -p output_folders_${k}/${lib}output; cd output_folders_${k}/${lib}output

# Copy generic config file to current directory and append the paths for left and right paired reads to config file
cp ../../config.txt ./${lib}config.txt
echo "q1=${q1}" >> ${lib}config.txt
echo "q2=${q2}" >> ${lib}config.txt


# Check if k-mer value is specified
if [[ -v k ]]
then

# Run with specific k-mer value
SOAPdenovo-Trans-127mer all \
-s ${lib}config.txt \
-o ${lib}${tree} \
-p 10 \
-K $k

else

# Run with default k-mer value
SOAPdenovo-Trans-127mer all \
-s ${lib}config.txt \
-o ${lib}${tree} \
-p 10

fi
