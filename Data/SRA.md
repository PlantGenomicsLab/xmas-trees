<h3>Commands needed to pull SRA data from NCBI:

``module load sratoolkit``

``prefetch SRRAccession``

``fastq-dump -I --split-lines SRRAccession``

Replace SRRAccession with the SRA accession to be downloaded. Ensure fastq-dump is ran in the directory where you want the files to go

Other useful links:

- https://www.biostars.org/p/232040/

- https://ncbi.github.io/sra-tools/fastq-dump.html 

- https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=toolkit_doc&f=vdb-validate 
