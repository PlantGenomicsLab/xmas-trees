#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=
#SBATCH -o orthofinder-%j.output
#SBATCH -e orthofinder-%j.error


module load OrthoFinder/2.4.0
module load muscle
module load DLCpar/1.0
module load FastME
module unload diamond
module load diamond/0.9.25
module load mcl
module load anaconda/4.4.0


orthofinder -f /labs/Wegrzyn/Xmas/OrthoFinder -S diamond -t 16

