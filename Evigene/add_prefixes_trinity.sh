#!/bin/bash
#SBATCH --job-name=addPrefixesTrinity
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o addPrefixesTrinity_%j.out
#SBATCH -e addPrefixesTrinity_%j.err

# Add prefixes to the FASTA headers

echo `hostname`

###
tree="balsam"
###

###
assembler="trinity"
###

# Directory with Trinity assemblies
trinitydir=/labs/Wegrzyn/Transcriptomics/xmas_trees/NAR/poola/transcriptomes/assemblies/${tree}

# Make file listing all library paths
libraries=${tree}_${assembler}_libraries.txt
ls $trinitydir/*.fasta > $libraries

# Make directory for new FASTA assemblies
prefixdir=${assembler}_prefix
mkdir $prefixdir

# Add prefixes to each FASTA file
while read lib
do
	## Edit $lib down to library name
	filename=${lib#$trinitydir/}
	lib_name=${filename%.Trinity.fasta}
	##
	sed "s/>/>${assembler}_${lib_name}_/g" $lib > $prefixdir/${lib_name}.prefix.transcripts.fasta
done < $libraries

# Discard library file
rm $libraries

