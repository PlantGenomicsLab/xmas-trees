Evigene Transcriptome Assembly

- [ ] Evigene is meant to take as input lots of transcripts, with lots of redundancy, and output a final, polished gene set
- [ ] Use as many transcriptome assemblers as you can, first (Trinity, rnaSPAdes, and SOAPdenovo-Trans have been used here)
- [ ] Use as many different RNA-Seq libraries as possible for each species



- How EvidentialGene (EviGene) works - http://arthropods.eugenes.org/EvidentialGene/trassembly.html
- http://arthropods.eugenes.org/EvidentialGene/trassembly.html
- How to get Best mRNA Transcript assemblies - http://arthropods.eugenes.org/EvidentialGene/evigene/docs/perfect-mrna-assembly-2013jan.txt
- Documentation isn't great





**1. Add prefixes to transcriptome assemblies**

`add_prefixes_trinity.sh` adds the library name to the beginning of each header in all the Trinity FASTA assemblies to prevent transcripts from getting mixed up
```
Input: Path to Trinity FASTA assemblies
Output Directory: trinity_prefix
```


`add_prefixes_spades.sh` - Same purpose
```
Input: Path to rnaSPAdes FASTA assemblies
Output Directory: spades_prefix
```


`add_prefixes_soap.sh`
```
Input: Path to SOAPdenovo-Trans FASTA assemblies
Output Directory: soap_prefix
```


**2. Concatenate and format input transcripts**

`evi_format.sh` uses `trformat.pl` to format and concatenate all transcripts for each assembler

```
Trinity
	Input: trinity_prefix/*
	Output: trinity.tr
Spades
	Input: spades_prefix/*
	Output: spades.tr
Soap
	Input: soap_prefix/*
	Output: soap.tr

Concatenates *.tr into out_dir/${tree}.cdna
```


**3. Run tr2aacds**

`tr2aacds.sh` uses `tr2aacds.pl` to create the gene set
```
Input: out_dir/${tree}.cdna
Output Directories: out_dir/okayset and out_dir/dropset contain cDNA, CDS, and proteins

out_dir/dropset contains "dropped" transcripts, should not be used.
out_dir/okayset contains both the Okayset and the Okaltset files - confusing
	Okayset: Best quality transcripts - Use this one
	Okaltset: Alternate transcripts
Use the "Okayset"

```



**4. Filter out <=300bp transcripts**

```
mkdir -p final/filtered
mkdir final/filtered-pep
```


`final/filtered/filter300.sh` uses `filterLen.py` to filter out transcripts <=300 bp
```
script=/labs/Wegrzyn/annotationtool/data-gathering/3_filter/filterLen.py
Input: out_dir/okayset/${tree}.okay.cdna
Output: final/filtered/${tree}-filtered.cdna
```

`final/filtered-pep/filterpep.sh` uses `createFasta.py` to filter out the protein transcripts that correspond to filtered out RNA transcripts
```
script=/labs/Wegrzyn/annotationtool/data-gathering/8_align_protein/createFasta.py
Input: out_dir/okayset/${tree}.okay.aa
Output: final/filtered-pep/${tree}-filtered.aa
```
