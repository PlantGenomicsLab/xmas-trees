#!/bin/bash
#SBATCH --job-name=tr2aacds
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=250G
#SBATCH -o tr2aacds_%j.out
#SBATCH -e tr2aacds_%j.err

# Evigene tr2aacds transcriptome assembler

echo `hostname`

###
tree="balsam"
###

module load perl/5.28.1
module load blast/2.10.1
module load exonerate/2.4.0

evigene=/isg/shared/apps/evigene/20190101
export PATH=$evigene/scripts/:$evigene/scripts/prot/:$evigene/scripts/rnaseq/:/isg/shared/apps/cdhit/4.6.8/:$PATH

cd out_dir

echo "Assembling transcriptome..."

# Specify memory in MB, make sure it matches mem. in header

tr2aacds.pl -tidy \
-NCPU 8 \
-MAXMEM 250000 \
-log \
-cdna ${tree}.cdna

# Directory cleanup
rm -r inputset dropset tmpfiles ${tree}_split ${tree}.cdna ${tree}nrcd1_*

echo "Transcriptome assembled!"
