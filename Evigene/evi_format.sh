#!/bin/bash
#SBATCH --job-name=EviFormat
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o EviFormat_%j.out
#SBATCH -e EviFormat_%j.err

# Compiles and formats all FASTA assemblies for each assembler

echo `hostname`

###
tree="balsam"
###

module load perl/5.28.1
module load blast/2.10.1
module load exonerate/2.4.0

evigene=/isg/shared/apps/evigene/20190101
export PATH=$evigene/scripts:$evigene/scripts/prot/:$evigene/scripts/rnaseq/:/isg/shared/apps/cdhit/4.6.8/:$PATH

assembler="spades"

prefixdir=${assembler}_prefix
trformat.pl -prefix $tree -log \
-output ${assembler}.tr \
-input $prefixdir/*.fasta


assembler="trinity"

prefixdir=${assembler}_prefix
trformat.pl -prefix $tree -log \
-output ${assembler}.tr \
-input $prefixdir/*.fasta


assembler="soap"

prefixdir=${assembler}_prefix
trformat.pl -prefix $tree -log \
-output ${assembler}.tr \
-input $prefixdir/*.fasta



mkdir out_dir
cat ./*.tr > out_dir/${tree}.cdna


#### WHOLE THING SHOULD TAKE ABOUT 2-4 MINUTES
