#!/bin/bash
#SBATCH --job-name=addPrefixesSpades
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o addPrefixesSpades_%j.out
#SBATCH -e addPrefixesSpades_%j.err

# Add prefixes to the FASTA headers

echo `hostname`

###
tree="balsam"
###

###
assembler="spades"
###


# Make file listing all library folder paths
libraries=${tree}_${assembler}_libraries.txt
ls -d $assembler/output_folders/* > $libraries

# Make directory for new FASTA assemblies
prefixdir=${assembler}_prefix
mkdir $prefixdir

# Add prefixes to each FASTA file
while read lib
do
	## Edit $lib down to library name
        folder_name=${lib#$assembler/output_folders/}
	lib_name=${folder_name%.output}
	##
	sed "s/>/>${assembler}_${lib_name}_/g" $lib/transcripts.fasta > $prefixdir/${lib_name}.prefix.transcripts.fasta
done < $libraries

# Discard library file
rm $libraries

