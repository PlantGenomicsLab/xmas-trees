
# Oyster River Protocol (ORP) for Transcriptome Assembly 
ORP assembles the transcriptome using a multi-kmer multi-assembler approach, then merges those assemblies into 1 final assembly. The manuscript corresponding to this protocol is here: https://peerj.com/articles/5428/
<br>
# Table of Contents
[[_TOC_]]

## 1. Running ORP
ORP was run against individual libraries of balsam (32), canaan (26) and fraser (35). 

<h3> Preliminary steps:

First, you need to create and edit two files in your /home/CAM/user directory using touch.

```
touch .bash_profile
nano .bash_profile
```

Once you use touch to create the file, you can use whichever text editor you would prefer. I prefer nano. Once in the .bash_profile file, paste this block of code in it.

```
PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/bin
PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/OrthoFinder/
PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/orp-transrate
PATH=$PATH:/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/transabyss
export TMPDIR=/labs/Wegrzyn/Xmas/temp/
```

Save the file and exit. Next step is the .bash_rc file.

```
touch .bash_rc
nano .bash_rc
```

And then paste this line of code into the .bash_rc file

`/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/etc/profile.d/conda.sh`

After that, you need to restart your shell. Once you restart your shell, enter an interactive session

`srun --partition=general --qos=general --pty bash`

and then run these sets of commands

```
conda activate
conda init bash
source activate orp
```

If everything worked correctly, you should have an (orp) to the left of your bash, and you can now successfully run ORP. Goto where the scripts you want to run are, and run them! After these steps, all you need to do in the future to run orp would be:

`source activate orp`

<h3> Script:

Below is what the script looks like for balsam library 1:

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=balsam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=110G
#SBATCH --mail-type=END
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o test_%j.out
#SBATCH -e test_%j.err

hostname
echo "\nStart time:"
date

/labs/Wegrzyn/local_software/Oyster_River_Protocol/oyster.mk main CPU=24 MEM=110 \
  STRAND=RF \
  READ1=/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R1.fastq\
  READ2=/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R2.fastq\
  LINEAGE=virdiplante_odb10 \
  RUNOUT=1_balsam

echo "\nEnd time:"
date
</pre> 

If the run was successful, the following files will be seen in the `assemblies` directory:
    1_balsam.filter.done
    1_balsam.flagstat
    1_balsam.ORP.diamond.txt
    1_balsam.ORP.fasta
    1_balsam.ORP.intermediate.fasta.clstr
    1_balsam.ORP.intermediate.fasta
    1_balsam.orthomerged.fasta
    1_balsam.spades55.fasta
    1_balsam.spades75.fasta
    1_balsam.transabyss.fasta
    1_balsam.trinity.Trinity.fasta
The most important file is the ORP fasta file:
    1_balsam.ORP.fasta

## 2. Frame Selection with TransDecoder
Because ORP does not remove coding regions, TransDecoder was run on each ORP.fasta file. To ensure that there were no duplicate headers amongst libraries, each was given a unique identifier using sed (shown below).

    sed 's/>/>1_/g' 1_balsam.ORP.fasta > 1.fasta 

With a unique ID, TransDecoder can now be run on:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=transdecoder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "transdecoder"

module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../1.fasta
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm 1.fasta.transdecoder_dir/longest_orfs.pep
TransDecoder.Predict -t ../1.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout 

</pre>

## 3. Clustering at 95% with VSEARCH
The TransDecoder nucleotide (CDS) files were concatenated into a single assembly:
    
    cat 1.fasta.transdecoder.cds 2.fasta.transdecoder.cds … > combine.cds 

And then base pairs less than 300 were removed in an interactive session with Seqtk:

    srun --partition=general --qos=general --mem=5g --pty bash
    Module load seqtk
    seqtk seq -L 300 combine.cds > combine_300.cds 
    
Following concatenation and filtering, VSEARCH was run:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 40
#SBATCH --mem=150G
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "vsearch"

module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
        --cluster_fast ../combine_300.cds \
        --id 0.95 \
        --centroids centroids.fasta \
        --uc clusters.uc 
</pre>

## 4. Contaminant Removal with EnTAP at 70/70 Coverage
Following clustering, the assembly was complete but still contained contaminants. To remove them several steps were taken:
#### 1. Create text file of all centroids.fasta headers:
    grep -h “>” centroids.fasta > centroids.txt
#### 2. Remove the “>” from list with:
    sed -i 's/^.//' centroids.txt 
#### 3. TransDecoder creates .pep files (we have been using .cds) - you need to concatenate ALL of the library peptide files in your TransDecoder directory and bring it to your working directory with centroids.txt file.
    cat *.pep > combine.pep
#### 4. Next, you need to isolate any centroids.txt headers in that peptide file
    srun --partition=general --qos=general --mem=5g --pty bash
    module load seqtk
    seqtk subseq combine.pep centroids.txt > combine_cluster.pep 
#### 5. Run EnTAP with the peptide file (change entap_config.ini if need be [i.e. taxon])

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 20
#SBATCH --mem=150G
#SBATCH --qos=general
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general

module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.25
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/EnTAP --runP --ini entap_config.ini -i combine_cluster.pep -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.200.dmnd --threads 20
</pre>

#### 6. Isolate contaminant headers in: entap_outfiles/similarity_search/DIAMOND/overall_results/best_hits_contam.faa
    grep -h “>” best_hits_contam.faa > contam.txt
    sed -i 's/^.//' contam.txt 
#### 7. Take that contam text file to your Clustering directory (where you keep centroids.fasta) and remove contaminants
    srun --partition=general --qos=general --mem=5g --pty bash
    module load seqkit
    seqkit grep -v -f contam.txt centroids.fasta > no_contam.fasta
Now you can run busco and rnaquast on the no_contam.fasta file! This is our FINAL assembly


## Busco
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=busco_balsam
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err


module load busco/5.0.0


busco -i /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/NEW/EnTAP/Contam_Removal/no_contam.fasta -l viridiplantae_odb10 -o buscoClustering_nocontam -m Transcriptome </pre>

## RNAQuast
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=rnaQuast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "rnaQUAST"

module load rnaQUAST/2.2.0
module load GeneMarkS-T/5.1

python /isg/shared/apps/rnaQUAST/2.2.0/rnaquast-2.2.0/rnaQUAST.py --transcripts /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/NEW/EnTAP/Contam_Removal/no_contam.fasta \
        --gene_mark \
        --threads 8 \
        --output_dir results_vsearch_nocontam

</pre>

## 5. Index and Counts with Kallisto
Each contam free fasta file was indexed with Kallisto using the following script:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=kallisto_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=40G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
##SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "kallisto_index"

module load kallisto/0.46.1

kallisto index -i balsam_index /labs/Wegrzyn/Xmas/Assembly/ORP_balsam/COMPLETE_ORP_ASSEMBLIES/NEW/EnTAP/Contam_Removal/no_contam.fasta
</pre>

Read counts were then extracted by Kallisto processing paired-ends FASTQ files (multiple scripts were used):
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=kallisto_counts_1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname

module load kallisto/0.46.1

kallisto quant -i balsam_index \
        -o 1\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/1.Final.R2.fastq


kallisto quant -i balsam_index \
        -o 2\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/2.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/2.Final.R2.fastq


kallisto quant -i balsam_index \
        -o 11\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/11.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/11.Final.R2.fastq

kallisto quant -i balsam_index \
        -o 12\
			-b 30 \
        -t 8 --pseudobam \
    		/labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/12.Final.R1.fastq /labs/Wegrzyn/Xmas/Trimmed_Reads/balsam/12.Final.R2.fastq


</pre>

## 6. Mapping Rates with Samtools
In order to see how the libraries mapped to the assembled transcriptome, pseudobam files from kallisto were run through samtools flagstat with the following commands using a bash script in the parent directory:
    
    module load samtools

    #Three digit directory
    for d in [0-9][0-9][0-9]
    do
    ( cd "$d" && samtools flagstat *.bam > map.txt )
    done

    #Two digit directory
    for d in [0-9][0-9]
    do
    ( cd "$d" && samtools flagstat *.bam > map.txt )
    done

    #One digit directory
    for d in [0-9]
    do
    ( cd "$d" && samtools flagstat *.bam > map.txt )
    done
    
## 7. Differential Expression Analysis with Sleuth
Following Kallisto, each library has a resultant abundance.h5 file. Each h5 file needs to be labeled with the sample number:
EXAMPLE: Sample 1 would be 1_abundance.h5
At this point, all files can be moved to your local directory. 
### Preparing Metadata TXT File
Before running Sleuth in RStudio, a metadata file needs to be constructed in Excel following this formula (EX: Library 1):

| sample | clone | quality | year | path |
| ------ | ------ | ------ | ------ | ------ |
| 1 | BF05 | G | 2013 | /path/to/file/ |

Make sure to include every library in this file and save it as: `metadata.txt`

### Preparing Transcript to Gene File
You will also need to create a transcript ID to gene text file using EnTAP results (/EnTAP/entap_outfiles/final_results/final_annotations_no_contam_lvl0.tsv). In Excel, islate the query ID and similarity search ID and create the following:
	
|target_id|  ss_gene|
| -----| -----|
|1_J687861.p1 | XP_012064674.1 |
|1_J687871.p1 | XP_020520950.1 |
| 1_J687883.p1 | XP_010246207.1 |
etc...

This file will be called: `ttg.txt`

### Running Sleuth in RStudio
Now to run Sleuth, the following script was used:
```R 
library("sleuth")
library("shiny")
setwd("/path/to/files/")
metadata <- read.table('./metadata.txt', sep='\t', header=TRUE, stringsAsFactors = FALSE)
ttg <- read.table ('./ttg.txt', sep='\t', header=TRUE, stringsAsFactors = FALSE, quote="")
so <- sleuth_prep(metadata, bootstrap_summary=TRUE, read_bootsrap_tpm=TRUE, target_mapping = ttg)
so <- sleuth_fit(so, ~quality + year, 'full')
so <- sleuth_fit(so, ~year, 'reduced')
so <- sleuth_lrt(so, 'reduced', 'full')
so <- sleuth_wt(so, 'qualityG', 'full')
results_table_lrt <- sleuth_results(so, 'reduced:full', 'lrt', show_all = FALSE)
lrt_significant <- dplyr::filter(results_table_lrt, qval <= 0.05)
results_table_wald <- sleuth_results(so, 'qualityG','full', test_type = 'wt')
wald_significant <- dplyr::filter(results_table_wald, qval <= 0.05)
sleuth_live(so)
```
The b (beta) column has the effect size in natural log.

## 8. OrthoFinder: Infering Orthogroups 
**1. Isolate contam free PEP files (file we used for EnTAP) (e.g. Balsam)**

    #Use the no_contam.fasta file we used following EnTAP and isolate headers
        grep -h “>” no_contam.fasta > no_contam.txt

    #Remove “>” from beginning
        sed -i 's/^.//' no_contam.txt 

    #Use the “combine.pep” file we made for EnTAP and isolate all the sequences that are contam free 
	    srun --partition=general --qos=general --mem=5g --pty bash
        module load seqtk
        seqtk subseq combine.pep no_contam.txt > b_no_contam.pep 

Repeat this with fraser and canaan

**2. Move files to single directory: /labs/Wegrzyn/Xmas/OrthoFinder**

**3. Label DE genes for filtering** 

Using seqtk and seqkit in an interactive session, the following commands were performed on all contam free peptide files:

    #Balsam
    #Isolate DE genes using query list from Sleuth
        seqtk subseq b_no_contam.pep DE_b.txt > DE_b.pep
    #Remove DE genes from contam free pep file produced earlier with same list
        seqkit grep -v -f DE_b.txt b_no_contam.pep > b_no_DE.pep
    #Add a unique species header
        sed 's/>/>Balsam_/g' b_no_DE.pep > b_header.pep
    #Isolate up-regulated genes (postive b value)
        seqtk subseq DE_b.pep up_b.txt > DE_b_up.pep
    #Isolate down-regulated genes (negative b value)
        seqtk subseq DE_b.pep down_b.txt > DE_b_down.pep
    #Add a unique header for UP and DOWN regulated genes
        sed 's/>/>UP_Balsam_/g' DE_b_up.pep > up_b_header.pep
        sed 's/>/>DOWN_Balsam_/g' DE_b_down.pep > down_b_header.pep
    #Concatenate the DE-free pep file with the up and down regulated files to produce a single input for OrthoFinder
        cat b_header.pep up_b_header.pep down_b_header.pep > balsam.pep

Repeat this with canaan and fraser
<br>
**Make sure only: balsam.pep, canaan.pep and fraser.pep are being run by OrthoFinder**

**4. Run OrthoFinder script:**
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=
#SBATCH -o orthofinder-%j.output
#SBATCH -e orthofinder-%j.error


module load OrthoFinder/2.4.0
module load muscle
module load DLCpar/1.0
module load FastME
module unload diamond
module load diamond/0.9.25
module load mcl
module load anaconda/4.4.0


orthofinder -f /labs/Wegrzyn/Xmas/OrthoFinder -S diamond -t 16
</pre>








